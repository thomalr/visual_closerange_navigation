import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
#from geometry_msgs.msg import Twist
import numpy as np
import cv2
from cv_bridge import CvBridge
#import time 



class Image_processor(Node):
    def __init__(self):
        super().__init__('Image_processor')
        
        #publish odometry
        #self.pub_odom = self.create_publisher(Twist, 'odom',1)
        #time.sleep(5)

        #initialize_param

        #subscriber
        self.camera_subs = self.create_subscription( 
                            Image, 
                            '/minerva/cameraleft/image_raw',
                            self.Image_processor_callback, 
                            1)
        self.bridge = CvBridge()
        
    def image_processing_callback(self, msg):
        frame = self.bridge.imgmsg_to_cv2(msg, "rgb8")
        height = msg.height
        width = msg.width
        self.number_of_pixels = height


        ##
        matrix_coefficients = np.mat([[1.0, 0.0, height / 2.0],
                                     [0.0, 1.0, width / 2.0],
                                     [0.0, 0.0, 1.0]])
        distortion_coefficients = np.mat([0.0, 0.0, 0.0, 0.0])
    
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)  # Change grayscale
        # Use 5x5 dictionary to find markers
        cv2.imshow(gray)

    def main(args=None):

        rclpy.init(args=args)

        ImageProcessor = Image_processor()

        rclpy.spin(ImageProcessor)

        ImageProcessor.destroy_node()

        rclpy.shutdown()
    if __name__ == '__name__':
        main()